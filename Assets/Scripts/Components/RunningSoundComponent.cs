﻿using System;
using UnityEngine;

public sealed class RunningSoundComponent : MonoBehaviour
{
    [SerializeField] PlaySound playSound;

    CharacterComponent characterComponent;

    public void Start()
    {
        characterComponent = GetComponent<CharacterComponent>();
    }

    public void Update()
    {
        switch (characterComponent.CurrentState)
        {
            case State.RunningToEnemy:
            case State.RunningFromEnemy:
                playSound.PlaySoundEffect("StepSound1");
                break;
            default:
                break;

        }
        characterComponent = GetComponent<CharacterComponent>();
    }
}