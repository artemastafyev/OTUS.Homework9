﻿using System;
using UnityEngine;

public sealed class DamageSoundComponent : MonoBehaviour
{
    [SerializeField] PlaySound playSound;

    public void Start()
    {
        GetComponent<HealthComponent>().OnHealthChanged += PlayDamageSound;
    }

    private void PlayDamageSound(int health)
    {
        if (health > 0)
        {
            playSound.PlaySoundEffect("DamageSound");
        }
        else
        {
            playSound.PlaySoundEffect("DeadSound");
        }
    }
}