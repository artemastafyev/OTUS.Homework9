﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationEvents : MonoBehaviour
{
    CharacterComponent character;
    [SerializeField] PlaySound playSound;

    void Start()
    {
        character = GetComponentInParent<CharacterComponent>();
        playSound = transform.parent.GetComponentInChildren<PlaySound>();
    }

    void ShootEnd()
    {
        character.SetState(State.Idle);
        character.AttackFinished();
    }

    void AttackEnd()
    {
        character.SetState(State.RunningFromEnemy);
    }

    void HitMoment()
    {
        playSound.PlaySoundEffect("HandHit");
    }
}
